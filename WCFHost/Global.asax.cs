﻿using System;
using Levl.InfrastructureFramework.DAL;

namespace Levl.WCFHost
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            EntityRepositoryFactory.Instance.GetType();

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            
            //EntityRepositoryFactory.Instance.RegisterRepository<EFRoom>(new RoomEFBaseRepository(new EFBoundChatContext("DefaultConnection")));

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}