﻿using System.Linq;
using System.ServiceModel;
using Lelv.Model.ChatBoundContext;
using Levl.DAL;
using Levl.Infrastructure.Configuration.Connection;
using Levl.Infrastructure.DAL;
using Levl.WCFInfrustructureHost;

namespace Levl.WCFHost.HostApi
{
    /*// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ApplicationApi" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ApplicationApi.svc or ApplicationApi.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)] 
    public class ApplicationApi : IApplicationApi
    {
        public void ResetConnection(WcfConnectionString conneceStrings)
        {
            ReSetRepositoryAndConnections(conneceStrings.ConnectionString);
        }

        public void ResetRepositories()
        {
            EntityRepositoryFactory.Instance.RegisterRepository<Room<Chat<Message>>>(new RoomRepository().Instance);
        }

        private void ReSetRepositoryAndConnections(ConnectionString connectionStrings)
        {
            var repositories = EntityRepositoryFactory.Instance.GetAllRegisteredRepositories().ToList();
            repositories.ForEach(@base =>
            {
                var repositoryEntities = @base.Instance.GetAllEntities().ToList();
                @base.ResetRepository(connectionStrings.Value);
                repositoryEntities.ForEach(@base.Add);
            });
                
        }
    }*/
}
