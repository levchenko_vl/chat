﻿using System.ServiceModel;
using Levl.WCFInfrustructureHost;

namespace Levl.WCFHost.HostApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IApplicationApi" in both code and config file together.
    [ServiceContract]    
    public interface IApplicationApi
    {
        [OperationContract]
        void ResetConnection(WcfConnectionString connectionStrings );

        [OperationContract]
        void ResetRepositories();
    }
}
