﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Lelv.Model.ChatBoundContext;
using Levl.DAL;
using Levl.InfrastructureFramework.Configuration.Connection;
using Levl.InfrastructureFramework.DAL;
using Levl.InfrastructureFramework.DomainBase;
using Levl.InfrastructureFramework.Helpers;
using Levl.WCFInfrustructureHost;

namespace Levl.WCFHost.HostApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RepositoryContracts" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RepositoryContracts.svc or RepositoryContracts.svc.cs at the Solution Explorer and start debugging.
    
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults=true)] 
    public class RepositoryContracts : IRepositoryContracts, IApplicationApi
    {

        public void ResetConnection(WcfConnectionString connectionStrings)
        {
            ResetRepositorysConnections(connectionStrings.ConnectionString);
        }

        public void ResetRepositories()
        {
            EntityRepositoryFactory.Instance.RemoveRegistredRepositories();
            EntityRepositoryFactory.Instance.RegisterRepository<Room<Chat<Message>>>(new RoomRepository().Instance);
        }

        internal void ResetRepositorysConnections(ConnectionString connectionStrings)
        {
            var repositories = EntityRepositoryFactory.Instance.GetAllRegisteredRepositories().ToList();
            repositories.ForEach(@base =>
            {
                var repositoryEntities = @base.Instance.GetAllEntities();
                if (repositoryEntities != null)
                {
                    var entities = repositoryEntities as List<EntityBase> ?? repositoryEntities.ToList();
                    @base.ResetRepository(connectionStrings.Value);
                    entities.ForEach(@base.Add);
                }
                else
                    @base.ResetRepository(connectionStrings.Value);
            });

        }
        internal EntityRepositoryBase GetRepositoryBaseForType(string typeName)
        {
            return EntityRepositoryFactory.Instance.GetRepositoryByType(ReflectionHelper.GetTypeByName(typeName));
        }

        public void Add(WcfEntityBase item, string typeName)
        {
            var repository = GetRepositoryBaseForType(typeName);
            repository.Add(item.Entity);
        }

        public void Remove(WcfEntityBase item, string typeName)
        {
            var repository = GetRepositoryBaseForType(typeName);
            repository.Remove(item.Entity);
        }

        public IEnumerable<WcfEntityBase> GetFirstEntities(long qauntity, string typeName)
        {
            var repository = GetRepositoryBaseForType(typeName);
            var entityCollection = repository.GetFirstEntities(qauntity).ToList();
            var wcfEntityCollection = new List<WcfEntityBase>();
            entityCollection.ForEach(entityBase => wcfEntityCollection.Add(new WcfEntityBase { Entity = entityBase}));
            return wcfEntityCollection;
        }

        public void Commit(string typeName)
        {
            var repository = GetRepositoryBaseForType(typeName);
            repository.Commit();
        }
    }
}
