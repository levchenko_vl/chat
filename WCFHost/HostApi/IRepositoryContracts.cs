﻿using System.Collections.Generic;
using System.ServiceModel;
using Levl.WCFInfrustructureHost;

namespace Levl.WCFHost.HostApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRepositoryContracts" in both code and config file together.
    [ServiceContract]
    public interface IRepositoryContracts
    {
        [OperationContract]
        void Add(WcfEntityBase item, string typeName);
        
        [OperationContract]
        void Remove(WcfEntityBase item, string typeName);
        
        [OperationContract]
        IEnumerable<WcfEntityBase> GetFirstEntities(long qauntity, string typeName);
        
        [OperationContract]
        void Commit(string typeName);
    }
}
