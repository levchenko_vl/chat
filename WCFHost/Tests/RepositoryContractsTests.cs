﻿using System;
using System.Linq;
using Lelv.Model.ChatBoundContext;
using Levl.DAL;
using Levl.InfrastructureFramework.Configuration.Connection;
using Levl.InfrastructureFramework.DAL;
using Levl.InfrastructureFramework.DomainBase;
using Levl.WCFHost.HostApi;
using Levl.WCFInfrustructureHost;
using NUnit.Framework;

namespace Levl.WCFHost.Tests
{
    [TestFixture]
    public class RepositoryContractsTests
    {
        private const string TestConnectionStringInitial = "Test Connection String Initial";
        private const string TestConnectionString = "Test Connection String";
        private const string TestRoomName = "Test Room Name";
        private  WcfConnectionString _testWcfConnectionString;
        private EntityRepositoryBase _testRepository;
        private RepositoryContracts _repositoryContracts = new RepositoryContracts();
            
        [OneTimeSetUp]
        void OnceSetUp_PerAllTests()
        {
            _repositoryContracts = new RepositoryContracts();
            
            _testWcfConnectionString = new WcfConnectionString
            {
                ConnectionString = new ConnectionString {Name = TestConnectionString, Value = TestConnectionString}
            };
        }

        [SetUp]
        void OnceSetUp_PerEachTest()
        {
            _testRepository = new RoomRepository().Instance;
            _testRepository.Instance.CreateRepository(TestConnectionStringInitial);
            var room = new Room<Chat<Message>>();
            room.Name = TestRoomName;
            _testRepository.Add(room);
            EntityRepositoryFactory.Instance.RegisterRepository<EntityBase>(_testRepository);
             
        }

        [TearDown]
        void OnceSetDown_PerEachTest()
        {
            EntityRepositoryFactory.Instance.RemoveRegistredRepositories();
        }

        [Test]
        public void ResetConnectionsTest_NewConnectionString_AppliedToAllRepositories_ReturnsTrue()
        {
            _repositoryContracts.ResetConnection(_testWcfConnectionString);

            Assert.IsTrue(String.Equals(_testRepository.Instance.Parameters,_testWcfConnectionString.ConnectionString.Value));
        }

        [Test]
        public void ResetRepositoriesTest_ReloadRepositoryFactoryFromDefaultConfiguration_ReturnsNullForEntityBase()
        {
            _repositoryContracts.ResetRepositories();
            Assert.IsNull(EntityRepositoryFactory.Instance.GetRepository<EntityBase>()); 
        }

        [Test]
        public void
            ReSetRepositoryAndConnectionsTest__EachRepositoryChangesConnectionParameter_ReturnsTrueIfInternalStorageWasntCurrupted
            ()
        {
            _repositoryContracts.ResetRepositorysConnections(_testWcfConnectionString.ConnectionString);
            
            Assert.IsNotEmpty(_testRepository.GetAllEntities().Where(entity => ((Room<Chat<Message>>) entity).Name.Equals(TestRoomName)));
            Assert.IsTrue(String.Equals(_testRepository.Instance.Parameters, _testWcfConnectionString.ConnectionString.Value));
        }

        [Test]
        public void GetRepositoryBaseForTypeTest_ReturnRegistredRepository()
        {
            var resultRepository = _repositoryContracts.GetRepositoryBaseForType(typeof(EntityBase).FullName);
            Assert.IsNotEmpty(resultRepository.GetAllEntities().Where(entity => ((Room<Chat<Message>>)entity).Name.Equals(TestRoomName)));
        }

        [Test]
        public void AddEntityToRepositoryTest_ReturnsAddedReposotory()
        {
            var testItem = new EntityBase();
            var typeName = typeof (EntityBase).FullName;
            _repositoryContracts.Add(new WcfEntityBase{Entity = testItem}, typeName);
            var repository = _repositoryContracts.GetRepositoryBaseForType(typeName);
            Assert.IsNotEmpty(repository.GetAllEntities().Where(entity => entity == testItem));
        }

        [Test]
        public void RemoveEntityFromRepositoryTest_ReternsRepositoryWithoutOneElement()
        {
            var typeName = typeof(EntityBase).FullName;
            var items = _testRepository.GetAllEntities().ToList();
            var countBeforeDelete = items.Count(); 
            var testItem = items.FirstOrDefault(entity => entity is Room<Chat<Message>>);
            var item = new WcfEntityBase {Entity = testItem};
            
            _repositoryContracts.Remove(item,typeName);
            Assert.IsEmpty(_testRepository.GetAllEntities().Where(entity => entity == testItem));
            Assert.AreEqual(countBeforeDelete - 1, _testRepository.GetAllEntities().Count());
        }
    }
}           