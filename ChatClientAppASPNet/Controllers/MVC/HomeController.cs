﻿using System.Web.Mvc;

namespace Levl.ChatClientAppASPNet.Controllers
{
    public class HomeController :  Controller
    {
        // 
        // GET: /Home/ 
        public string Index()
        {
            return "Welcome to chat!";
        }
    }
}