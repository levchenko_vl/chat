﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Lelv.Model.ChatBoundContext;
using Levl.ChatClientAppASPNet.ApplicationApi;
using Levl.ChatClientAppASPNet.Models;
using Message = Lelv.Model.ChatBoundContext.Message;

namespace Levl.ChatClientAppASPNet.Controllers
{
    public class RoomController : Controller
    {
        // 
        // GET: /Room/ 
        public ActionResult Rooms()
        {
            //var rooms = new List<Room> {new Room() {Name = "Game"}, new Room() {Name = "Programming"}};

            //var roomsRepository = new WCFHost.RepositoryContractsClient().(typeof (Room));//EntityRepositoryFactory.Instance.GetRepository<EFRoom>();
            var rooms = new RepositoryContractsClient().GetFirstEntities(10, typeof(Room<Chat<Message>>).FullName).ToList();
            var roomsForView = new List<Room>();
            foreach (var room in rooms)
            {
                roomsForView.Add(new Room { Name = ((Room<Chat<Message>>)room.Entity).Name });
            }
            return View(roomsForView);
        }
        
    }
}
