﻿var app = angular.module('AccountApp', ['ngRoute', 'AccountModule']);

app.config(function ($routeProvider) {

    $routeProvider.when("/login", {
        controller: "accountLoginController",
        templateUrl: "/components/account/views/login.html"
    });

    $routeProvider.when("/signup", {
        controller: "acccountSignupController",
        templateUrl: "/app/views/signup.html"
    });

    $routeProvider.otherwise({ redirectTo: "/home" });
});

app.run(['accountService', function (accountService) {
    accountService.fillAuthData();
}]);