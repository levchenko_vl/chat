﻿using System;
using System.Linq;
using System.Web.Configuration;
using System.Web.Routing;
using ChatClientAppASPNet;
using Levl.ChatClientAppASPNet.ApplicationApi;
using Levl.InfrastructureFramework.Configuration.Connection;

namespace Levl.ChatClientAppASPNet
{
    public class Global : System.Web.HttpApplication
    {
        private readonly string DefaultConfigName = "DefaultConnection";
        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            var wcfApplicationApi = new ApplicationApiClient();
            wcfApplicationApi.ResetRepositories();
            var connectionString =
                ConnectionManager.GetConnectionStrings(WebConfigurationManager.ConnectionStrings)
                .FirstOrDefault(s => String.Equals(s.Name, DefaultConfigName));
            var wcfConnectionString = new WcfConnectionString();
            wcfConnectionString.ConnectionString = connectionString;
            wcfApplicationApi.ResetConnection(wcfConnectionString);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}