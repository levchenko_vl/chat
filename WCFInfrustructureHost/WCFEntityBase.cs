﻿using System.Runtime.Serialization;
using Lelv.Model.ChatBoundContext;
using Levl.InfrastructureFramework.DomainBase;

namespace Levl.WCFInfrustructureHost
{
    [DataContract]
    [KnownType(typeof(Room<Chat<Message>>))]
    public class WcfEntityBase : EntityBase
    {
        [DataMember]
        public EntityBase Entity { get; set; }
        
    }
}