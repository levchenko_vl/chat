﻿using System.Runtime.Serialization;
using Levl.InfrastructureFramework.Configuration.Connection;

namespace Levl.WCFInfrustructureHost
{
    [DataContract]
    public class WcfConnectionString 
    {
        [DataMember]
        public ConnectionString ConnectionString { get; set; }
    }
}