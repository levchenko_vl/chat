﻿using Levl.InfrastructureFramework.DomainBase;

namespace Model.ChatBoundContext
{
    public interface IMassageObject
    {
        ValueEntity Object { get; set; }
    }
}
