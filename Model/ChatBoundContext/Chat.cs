﻿using System;
using System.Collections.Generic;
using Levl.InfrastructureFramework.DomainBase;

namespace Lelv.Model.ChatBoundContext
{
    [Serializable]
    public class Chat<TInMessage> : ReferenceEntity where TInMessage : Message
    {
        public string Name { get; set; }

        public virtual ICollection<TInMessage> Messages { get; set; } 

        public static implicit operator Chat<Message>(Chat<TInMessage> chat)
        {
            throw new NotImplementedException();
        }
    }
}
