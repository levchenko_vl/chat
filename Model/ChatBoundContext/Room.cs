﻿using System;
using System.Collections.Generic;
using Levl.InfrastructureFramework.DomainBase;

namespace Lelv.Model.ChatBoundContext
{
    [Serializable]
    public class Room<TInTChat> : ReferenceEntity
    {
        public string Name { get; set; }
        public virtual ICollection<TInTChat> Chats { get; set; }
    }
}
