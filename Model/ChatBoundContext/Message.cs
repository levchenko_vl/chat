﻿using System;
using System.Collections.Generic;
using Levl.InfrastructureFramework.DomainBase;
using Model.ChatBoundContext;

namespace Lelv.Model.ChatBoundContext
{
    [Serializable]
    public class Message : ReferenceEntity
    {
        public long FromUserId { get; set; }
        public ICollection<long> ToUserList { get; set; }
        public ICollection<IMassageObject> MessageObjectList { get; set; }
        public DateTime Time { get; set; }

    }
}
