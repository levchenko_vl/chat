﻿using System;
using System.Configuration;

namespace Levl.InfrastructureFramework.Configuration.Connection
{
    [Serializable]
    public class ConnectionString
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public static implicit operator ConnectionString(ConnectionStringSettings connectionStringSettings)
        {
            return new ConnectionString { Name = connectionStringSettings.Name, Value = connectionStringSettings.ConnectionString };
        }
    }
}
