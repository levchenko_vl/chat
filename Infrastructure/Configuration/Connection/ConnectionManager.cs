﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Levl.InfrastructureFramework.Configuration.Connection
{
    public static class ConnectionManager
    {
        public static IEnumerable<ConnectionString> GetConnectionStrings(ConnectionStringSettingsCollection connectionStringSettings)
        {
            var connectionStrings = connectionStringSettings
                .Cast<ConnectionStringSettings>()
                .Select(settings => (ConnectionString)settings);
            return connectionStrings;
        }
    }
}
