﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Configuration;
using Levl.InfrastructureFramework.Configuration.Connection;
using NUnit.Framework;


namespace Levl.InfrastructureFramework.Tests.UnitTests
{
    [TestFixture]
    public class ConnectionStringTest
    {
        private static readonly string TestConfigName = "TestConnection";
        private static readonly string TestConfigString = "Test Connection string";

        [Test]
        public void ReadConnectionString_From_TestWebConfigFile_Returns_True()
        {
            var connectionString =
                ConnectionManager.GetConnectionStrings(WebConfigurationManager.ConnectionStrings).FirstOrDefault(s => String.Equals(s.Name, TestConfigName));
            Assert.IsNotNull(connectionString);
            Assert.IsTrue(connectionString.Value.Contains(TestConfigString));
        }

        [Test]
        public void ConnectionStringSettings_ImplicitConversionTo_ConnectionString_Returns_True()
        {
            var testConnectionStringSettings = new ConnectionStringSettings(TestConfigName, TestConfigString);

            ConnectionString connectionString = testConnectionStringSettings;
            Assert.IsNotNull(connectionString);
            Assert.IsTrue(String.Equals(connectionString.Name,TestConfigName));
            Assert.IsTrue(String.Equals(connectionString.Value,TestConfigString));
        }

        [Test]
        public void ConnectionStringSettingsCollection_ImplicitConversionTo_ConnectionStringCollection_Returns_True()
        {
            var testConnectionStringSettingsList = new List<ConnectionStringSettings>();
            for (int i = 1; i <= 3; i++)
            {
                testConnectionStringSettingsList.Add(new ConnectionStringSettings
                {
                    Name = TestConfigName + i,
                    ConnectionString = TestConfigString
                });
            }

            var connectionStrings = testConnectionStringSettingsList.Select(settings => (ConnectionString)settings);
            Assert.IsNotNull(connectionStrings);

            var testConnectionStringArray = connectionStrings as ConnectionString[] ?? connectionStrings.ToArray();
            for (int i = 0; i < 3; i++)
            {
                Assert.IsTrue(String.Equals(testConnectionStringArray[i].Name, TestConfigName + (i+1)));
                Assert.IsTrue(String.Equals(testConnectionStringArray[i].Value, TestConfigString));
        
            }
           
        }
    }
}
