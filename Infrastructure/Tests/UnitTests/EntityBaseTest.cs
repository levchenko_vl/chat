﻿using System;
using Levl.InfrastructureFramework.DomainBase;
using NUnit.Framework;

namespace Levl.InfrastructureFramework.Tests.UnitTests
{
    public class EntityBaseTest
    {
        [Test]
        public void EntityBaseTest_TwoEntitiesAreEqualWithOneKey_ReturnsTrue()
        {
            var key =(object) new Guid();
            var entityBase1 = new EntityBase(key);
            var entityBase2 = new EntityBase(key);

            Assert.True(entityBase1 == entityBase2);
            Assert.True(entityBase1.Equals(entityBase2));
         }
    }
}
