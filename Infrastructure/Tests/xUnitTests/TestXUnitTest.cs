﻿namespace Levl.InfrastructureFramework.Tests.xUnitTests
{
    using DAL;
    using DomainBase;
    using System;
    using Xunit;
    using System.Linq;

    public class TestXUnitTest
    {
        [Fact]
        public void EntityRepositoryFactoryTest_IntanceIsCreatedOnlyOnce_ReturnsTrue()
        {
            var entityRepositoryFactory = new EntityRepositoryFactory();
            var repositoriesList = entityRepositoryFactory.GetAllRegisteredRepositories();

            Assert.Empty(repositoriesList);
            var _testRepository = new FakeEntityRepositoryBase().Instance;
            entityRepositoryFactory.RegisterRepository<EntityBase>(_testRepository);

            repositoriesList = entityRepositoryFactory.GetAllRegisteredRepositories().ToList();
            Assert.True(repositoriesList.Count() == 1);
            Assert.Equal(repositoriesList.FirstOrDefault(), _testRepository);
        }

        [Fact]
        public void EntityBaseTest_TwoEntitiesAreEqualWithOneKey_ReturnsTrue()
        {
            var key = (object)new Guid();
            var entityBase1 = new EntityBase(key);
            var entityBase2 = new EntityBase(key);

            Assert.True(entityBase1 == entityBase2);
            Assert.True(entityBase1.Equals(entityBase2));
        }
    }
}
