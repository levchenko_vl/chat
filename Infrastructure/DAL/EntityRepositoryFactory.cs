﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Levl.InfrastructureFramework.DomainBase;
using Levl.InfrastructureFramework.Helpers;

namespace Levl.InfrastructureFramework.DAL
{
    public sealed class EntityRepositoryFactory : IEntityRepositoryFactory<EntityRepositoryBase, EntityRepositoryBase>
    {
        private static EntityRepositoryFactory _instance;
        private static readonly object SyncRoot = new Object();
        private volatile Dictionary<Type, EntityRepositoryBase> _repositories;

        private Dictionary<Type, EntityRepositoryBase> Repositories
        {
            get { return Instance._repositories; }
        }

        public static EntityRepositoryFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new EntityRepositoryFactory
                            {
                                _repositories = new Dictionary<Type, EntityRepositoryBase>()
                            };
                        }
                    }
                }

                return _instance;
            }
        }

        public void RegisterRepository<TInType>(EntityRepositoryBase repositoryBase) where TInType : EntityBase
        {
            if (Instance.GetRepository<TInType>() == null)
            {
                Instance.Repositories.Add(typeof (TInType), repositoryBase);
            }
        }

        public EntityRepositoryBase GetRepository<TInType>() where TInType : EntityBase
        {
            return GetRepositoryByType(typeof (TInType));
        }

        public void RemoveRegistredRepositories()
        {
            Repositories.Clear();
        }

        public EntityRepositoryBase GetRepositoryByType(Type type)
        {
            var repository = Instance.Repositories.ContainsKey(type)? Instance.Repositories[type] : null;
            repository = repository ?? GetRepositoryByBaseModelType(type);
            repository = repository ?? GetDefaultrepository();
            
            return repository;
        }

        private EntityRepositoryBase GetDefaultrepository()
        {
            return null;
        }

        private EntityRepositoryBase GetRepositoryByBaseModelType(Type descendantType)
        {
            List<Assembly> assemblies =
                ReflectionHelper.GetAssemblysByNames(ReflectionHelper.ModelAssemblysNames).ToList();
            var modelTypes = new List<Type>();
                assemblies.ForEach(
                    assembly =>
                        modelTypes.AddRange(assembly.GetTypes()
                            .Where(type => type.IsSubclassOf(typeof (EntityBase)) && type.IsPublic)
                            .ToList()));
            var baseModelTypesForDescendant = modelTypes.Where(descendantType.IsSubclassOf);
            var baseType = baseModelTypesForDescendant.FirstOrDefault(type => descendantType.BaseType == type);
            
            if (baseType != null && Instance.Repositories.ContainsKey(baseType))
                return Instance.Repositories[baseType];

            return null;
        }

        public IEnumerable<EntityRepositoryBase> GetAllRegisteredRepositories()
        {
            return Instance.Repositories.Select(pair => pair.Value);
        }
    }
}
