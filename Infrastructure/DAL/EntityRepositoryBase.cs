﻿using System;
using System.Collections.Generic;
using System.Linq;
using Levl.InfrastructureFramework.DomainBase;

namespace Levl.InfrastructureFramework.DAL
{
    [Serializable]
    public class EntityRepositoryBase : IEntityRepository<EntityBase, EntityBase>
    {
        private volatile EntityRepositoryBase _instance;

        private IList<EntityBase> _entities;

        private string _parameters;

        protected EntityRepositoryBase()
        {
        }

        public EntityRepositoryBase Instance
        {
            get { return _instance ?? (_instance = this); }
        }

        public string Parameters {
            get { return _parameters; }
        }

        public virtual void CreateRepository(string parameters = null)
        {
            _parameters = parameters;
            _entities = new List<EntityBase>();
        }

        public virtual void ResetRepository(string parameters)
        {
            CreateRepository(parameters);
        }

        public virtual void Add(EntityBase item)
        {
            Instance._entities.Add(item);
        }

        public virtual void Remove(EntityBase item)
        {
            Instance._entities.Remove(item);
        }

        public IEnumerable<EntityBase> GetAllEntities()
        {
            return Instance._entities;
        }

        public virtual IEnumerable<EntityBase> GetFirstEntities(long qauntity)
        {
            return Instance._entities.Take((int)qauntity);
        }

        public virtual void Commit()
        {
            throw new NotImplementedException();
        }
    }
}
