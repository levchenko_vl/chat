﻿using System.Collections.Generic;
using Levl.InfrastructureFramework.DomainBase;

namespace Levl.InfrastructureFramework.DAL
{
    public interface IEntityRepository<in TInT, out TOutT>
        where TOutT : EntityBase
        where TInT : EntityBase
    {
        //T FindByKey(object key);
        void Add(TInT item);
        void Remove(TInT item);
        IEnumerable<TOutT> GetAllEntities();
        IEnumerable<TOutT> GetFirstEntities(long qauntity);
        void Commit();

    }
}
