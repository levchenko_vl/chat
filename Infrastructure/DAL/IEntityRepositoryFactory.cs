﻿using Levl.InfrastructureFramework.DomainBase;

namespace Levl.InfrastructureFramework.DAL
{
    public interface IEntityRepositoryFactory<in TInRepository, out TOutRepository>
        where TInRepository : IEntityRepository<EntityBase, EntityBase>
    {
        void RegisterRepository<TInType>(TInRepository repositoryBase) where TInType : EntityBase;
        TOutRepository GetRepository<TInType>() where TInType : EntityBase;
        void RemoveRegistredRepositories();
    }
}
