﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Levl.InfrastructureFramework.Helpers
{
    public static class ReflectionHelper
    {
        public const string ModelAssemblysNames = "Levl.Model,Levl.InfrastructureFramework";

        /*static ReflectionHelper()zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz4rh2hhhhhhhhhhhhhhhhhhhh
        {
            Assembly.Load(ModelAssemblyName);
        }*/

        public static Type GetTypeByName(string typeName)      
        {
            var modelType = ModelType(typeName);
            return modelType;
        }

        private static Type ModelType(string typeName)
        {
            Type modelType = null;
            var assemblies = GetAssemblysByNames(ModelAssemblysNames);
            foreach (var assembly in assemblies)
            {
                modelType = assembly.GetType(typeName);
                if (modelType !=null)
                {
                    break;
                }
            }
            return modelType;
        }

        internal static IEnumerable<Assembly> GetAssemblysByNames(string assemblysNames)
        {
            var assemblies = assemblysNames.Split(new []{','}).ToList().Select(Assembly.Load);
            return assemblies;
        }

    }
}