﻿using System;

namespace Levl.InfrastructureFramework.DomainBase
{
    [Serializable]
    public class EntityBase
    {
        /// <summary>
        ///     Key is used to check object identity
        /// </summary>
        private readonly object _key;

        /// <summary>
        ///     Default constructor
        /// </summary>
        public EntityBase() : this(null)
        {
        }

        public EntityBase(object key)
        {
            _key = key;
        }

        public object Key
        {
            get { return _key; }
        }

        public override bool Equals(object entity)
        {
            if (!(entity is EntityBase))
            {
                return false;
            }
            return (this == (EntityBase) entity);
        }

        public static bool operator ==(EntityBase entity1, EntityBase entity2)
        {
            if ((object) entity1 == null && (object) entity2 == null)
            {
                return true;
            }

            if ((object) entity1 == null || (object) entity2 == null)
            {
                return false;
            }

            if (entity1.Key != entity2.Key)
            {
                return false;
            }
            return true;
        }

        public static bool operator !=(EntityBase entity1, EntityBase entity2)
        {
            return !(entity1 == entity2);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }
    }
}