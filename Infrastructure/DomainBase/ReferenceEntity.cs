﻿using System;

namespace Levl.InfrastructureFramework.DomainBase
{
    [Serializable]
    public class ReferenceEntity : EntityBase
    {
        public ReferenceEntity() : base(Guid.NewGuid())
        {
            
        }
    }
}
