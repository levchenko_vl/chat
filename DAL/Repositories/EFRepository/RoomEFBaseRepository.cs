﻿using System.Data.Entity;
using Levl.DAL.Repositories.EFRepository.EFTypes;

namespace Levl.DAL.Repositories.EFRepository
{
    public class RoomEFBaseRepository : EFBaseRepository<EFRoom>
    {
        public RoomEFBaseRepository(DbContext dbContext) : base(dbContext)
        {
            for (long i = 0; i < 100; i++)
            {
                Add(new EFRoom { Name = "Room" + i });
            }
            Commit();
        }
    }
}
