﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Levl.DAL.Repositories.EFRepository.EFTypes;

namespace Levl.DAL.Repositories.EFRepository
{
    public class EFBoundChatContext : DbContext
    {
        public EFBoundChatContext(string connectionString):base(connectionString){}

        public DbSet<EFRoom> Rooms { get; set; }
        public DbSet<EFChat> Chats { get; set; }
        public DbSet<EFMessage> Messages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<EFRoom>().HasKey<int>(s => s.Id);
            modelBuilder.Entity<EFChat>().HasKey<int>(s => s.Id);
            modelBuilder.Entity<EFMessage>().HasKey<int>(s => s.Id);

            modelBuilder.Entity<EFChat>()
                    .HasRequired<EFRoom>(s => s.Room) // Student entity requires Standard 
                    .WithMany(s => s.Chats); // Standard entity includes many Students entities
            modelBuilder.Entity<EFRoom>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            
            modelBuilder.Entity<EFMessage>()
                    .HasRequired<EFChat>(s => s.Chat) // Student entity requires Standard 
                    .WithMany(s => s.Messages); // Standard entity includes many Students entities
            modelBuilder.Entity<EFChat>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //Configure Null Column
            //modelBuilder.Entity<Room>().Property(p => p.Chats).IsOptional();
        }
    }
}
