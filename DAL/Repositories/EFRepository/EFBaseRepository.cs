﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Levl.InfrastructureFramework.DAL;
using Levl.InfrastructureFramework.DomainBase;

namespace Levl.DAL.Repositories.EFRepository
{
    public class EFBaseRepository<TEntity> : EntityRepositoryBase where TEntity : EntityBase
    {
        private readonly DbContext DbContext = null;

        public EFBaseRepository(DbContext dbContext)
        {
            DbContext = dbContext;
        }
        public override void Add(EntityBase item)
        {
            DbContext.Set<TEntity>().Add((TEntity)item);
        }

        public override void Remove(EntityBase item)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<EntityBase> GetFirstEntities(long qauntity)
        {
             return DbContext.Set<TEntity>().Take((int)qauntity);
        }

        public override void Commit()
        {
            DbContext.SaveChanges();
        }
    }
}
