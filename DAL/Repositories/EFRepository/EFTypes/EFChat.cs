﻿using System.Collections.Generic;
using Lelv.Model.ChatBoundContext;
using Model.ChatBoundContext;

namespace Levl.DAL.Repositories.EFRepository.EFTypes
{
    public class EFChat : Chat<EFMessage>, IEFBase
    {
        public EFChat()
        {
            Id = Key.GetHashCode();
        }
        public int Id { get; private set; }

        public EFRoom Room { get; set; }

        public override ICollection<EFMessage> Messages { get; set; }

        public static Chat<EFMessage> ToChat(EFChat chat)
        {
            return new Chat<EFMessage>();
        }
    }
}
