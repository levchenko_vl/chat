﻿using Lelv.Model.ChatBoundContext;

namespace Levl.DAL.Repositories.EFRepository.EFTypes
{
    public class EFMessage : Message,IEFBase
    {
        public EFMessage()
        {
            Id = Key.GetHashCode();
        }
        public int Id { get; private set; }

        public EFChat Chat { get; set; } 
    }
}
