﻿using System.Collections.Generic;
using Lelv.Model.ChatBoundContext;
using Model.ChatBoundContext;

namespace Levl.DAL.Repositories.EFRepository.EFTypes
{
    public class EFRoom : Room<EFChat>, IEFBase
    {
        public EFRoom()
        {
            Id = Key.GetHashCode();
        }
        public int Id { get; private set; }
        public override ICollection<EFChat> Chats { get; set; }
    }
}
