﻿namespace Levl.DAL.Repositories.EFRepository.EFTypes
{
    public interface IEFBase 
    {
        int Id { get; }
    }
}
