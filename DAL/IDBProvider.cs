﻿using System.Data;

namespace Levl.DAL
{
    public interface IDbProvider
    {
        IDbCommand CreateCommand();

        IDbConnection CreateConnection();

        IDataReader CreateDataReader();

        IDataAdapter CreDataAdapter();

        IDbDataAdapter CreateDataAdapter();

        IDbDataParameter CreateParameter();

    }
}
