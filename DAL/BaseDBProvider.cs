﻿using System.Data;

namespace Levl.DAL
{
    public class BaseDbProvider : IDbProvider
    {
        public void Initialize()
        {
            
        }
        public IDbCommand CreateCommand()
        {
            throw new System.NotImplementedException();
        }

        public IDbConnection CreateConnection()
        {
            throw new System.NotImplementedException();
        }

        public IDataReader CreateDataReader()
        {
            throw new System.NotImplementedException();
        }

        public IDataAdapter CreDataAdapter()
        {
            throw new System.NotImplementedException();
        }

        public IDbDataAdapter CreateDataAdapter()
        {
            throw new System.NotImplementedException();
        }

        public IDbDataParameter CreateParameter()
        {
            throw new System.NotImplementedException();
        }
    }
}
