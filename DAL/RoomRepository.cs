﻿using Lelv.Model.ChatBoundContext;
using Levl.InfrastructureFramework.DAL;
using Model.ChatBoundContext;

namespace Levl.DAL
{
    public class RoomRepository : EntityRepositoryBase
    {
        public override void CreateRepository(string parameters = null)
        {
            base.CreateRepository(parameters);
            for (long i = 0; i < 100; i++)
            {
                Add(new Room<Chat<Message>>() { Name = "Room" + i });
            }
        }

    }
}
